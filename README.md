# Project specification

## Quick Intro

Clip board manager is a cross-platform application which helps to manage your 
copy-pastes. It supports your own snippets and is highly customizable when it
comes to the style. 

## Functional Requirements

- App is installable through Python package manager (PIP).
- When user starts the app, it works in background.
- Customizing or adding a snippet by user should be accessible via tray icon or 
using assigned keyboard shortcut.
- User should be allowed to change:
    - Amount of stored items in his clip board
    - Iconset
    - Keyboard shortcut of selecting from clip board
- Import/Export of settings, preferences, snippets and clipboard.

## Technical requirements
- App needs to be cross-platform. 
- App has to contain functional test suit.
- App needs to be modular, so it can be developed in multiple directions.


## Expansion Intentions

- Sharing clipboard across multiple devices.
- synchronizing settings, preferences and snippets. 
- Provide data-encryption, to store passwords from other services.
- ScreenShot manager.

